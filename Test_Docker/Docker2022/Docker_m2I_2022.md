# DOCKER

[lien teams](https://smex-ctp.trendmicro.com/wis/clicktime/v1/query?url=https%3a%2f%2fteams.microsoft.com%2fl%2fmeetup%2djoin%2f19%253ameeting%5fNzdiNTllZjEtMjM4Ny00MDU3LWE5YzgtYTQyMmI3ZmFhMDAy%2540thread.v2%2f0%3fcontext%3d%257b%2522Tid%2522%253a%2522373016f8%2d79a9%2d4eed%2d80d2%2d100ce948d960%2522%252c%2522Oid%2522%253a%2522234ea95c%2dfcf9%2d4835%2dbddb%2d6863e1a8007a%2522%257d&umid=ad8aad39-a545-4bfb-9f06-d79eee1be7d0&auth=19c58196df39d30a6dd633feb62314953e898d36-90b0ff42882582205021605fca92eb8f91009f18)
[EMERGEMENT](https://sign.m2iformation.fr/)
SESSION: SE22-182866
MdPasse: MeC866@
code pin: 48892
[liens vers la machine de tp](https://rdpnanterre.m2iformation.info/)
ID machine de TP: S15E.Dalifol
pass machine de TP: Formation@!


## PRESENTATION

* Les **namespaces** permettent l'isolation de ressources
namespace pid (gere les processus), net (), ipc (), ....

* Les **cgroups** permettent de définir des seuils de ressources
Ils se chargent de la consommaton de cpu, mémoire, i/o disk...

* Les **Unions File System**: génère les couches pour un fichier
Il gère les layers


Docker permet de générer facilement du micro service en démultipliant chaque composant dans un container.

## ARCHITECTURE


**docker machine**    permet de gérer les hosts sur la machine locale, le cloud...
**docker compose**    permet de gérer un ensemble de containers et leurs dependances en 1 fichier
**docker swarm**      permet de gérer un cluster de hosts qui gère des containers
**docker bundle**     permet de gérer les livrables (systeme de packaging). Il va remplacer docker-compose


## CYCLE DE VIE D'UN CONTAINER

`docker create`     crée un container mais ne le demarre pas
`docker start`      démarre un container s'il est créé

`docker run`        est un raccourci des 2 commandes precedentes

`docker kill`       tue le process du container
`docker stop`       arrête le container en demandant au processus de s'arrêter

`docker rm`         supprime un container quelquesoit son état (donc tue le process s'il est running)

syntaxe:
`docker run <image> <commande>`
`docker run ubuntu /bin/sh -c "echo hello"`
pour cette action on peut faire aussi
`docker run ubuntu echo hello`


## COMMANDES DOCKER CLI

`docker inspect <IDContainer>`          affiche les informations sur le container
`docker logs <IDContaine>`              affiche les logs
`docker logs -f <IDContaine>`           affiche les logs en temps réel (comme `tail -f`)
`docker exec <IDContainer> <commande>`  permet d'accéder à 1 container en running
`docker system prune`                   supprime toutes les images d'un coup


## LES IMAGES

Une **image** est un ensemble de fichiers qui a accès au processus pour démarrer le container.
Une image est composée de **layers**.
Chaque layer peut être réutilisée plusieurs fois et dans plusieurs images. Cela permet de ne pas à avoir à retélécharger la même couche plusieurs fois.
Une image fait le lien entre ces images pour le fonctionnement du container.

En général la 1ere couche est l'OS de base du systeme que nous allons utiliser.
Ce layer est en lecture seule.
Le couche en read/write est la couche applicative (exemple qui permet de modifier une conf spécifiquement....)
Cette couche est la partie la plus haute.

Il est possible d'utiliser une image avec `docker commit <IDContaine> <nomImage> --message "message du commit"`. Cela permet de créer une image en read-only la couche read-wrtite que nous avions.

On nomme notre image de la manière suivante `docker commit <IDContainer> mdali/<image001>`


## DOCKERFILES

`RUN` c'est du build de la construction
`CMD` ou `ENTRYPOINT` c'est une exécution à lancer

#### Actions de création d'une image à partir d'un dockerfile


```bash
vi Dockerfile   #dans notre cas on est sur le Dockerfile2
##Remarque:
### pour le ADD il faut mettre une virgule même après le dernier fichier à copier
### pour le ENTRYPOINT si on met les commandes sous forme de tableau, mettre des virgules entre les parametres et mettre chaque argument entre guillemets (sous forme string)

## syntaxe
### docker build -f <fichierDockerfile> --tag <nomImage> <context>
docker build -f .\Dockerfile2  --tag mynode_dockerfile .
docker images
------------------------
docker images
REPOSITORY          TAG       IMAGE ID       CREATED          SIZE
mynode_dockerfile   latest    28066e06c5ab   22 minutes ago   1GB
node_commited       latest    951363b76f50   50 minutes ago   998MB
node                latest    057129cb5d6f   11 days ago      998MB
nginx               latest    55f4b40fe486   11 days ago      142MB
------------------------
docker ps #pour vérifier que tout est OK avant de lancer le container
docker run -d -p 8080:8080 mynode_dockerfile
```



## EXPORTER UNE IMAGE SUR NOTRE DOCKER-HUB

```bash
## on crée un tag de l'image créée
docker tag 28066e06c5ab mdalifol/mynode_dockerfile

## on se connecte à docker-hub
docker login

## on push l'image sur docker-hub
docker push mdalifol/mynode_dockerfile


## pour récupérer l'image depuis docker-hub
docker pull mdalifol/mynode_dockerfile
```

## CREER UN REGISTRY EN LOCAL
[documentation docker regitry](https://docs.docker.com/registry/deploying/#running-on-localhost)


```bash
## on crée une registry en local
docker run -d -p 5000:5000 --restart=always --name mdali_registry registry:2

## on crée une image sur cette registry
docker tag 28066e06c5ab localhost:5000/mynode_dockerfile

## on push sur la registry qu'on vient de créer
docker push localhost:5000/mynode_dockerfile
### si on a dans la réponse un sha1 c'est que c'est OK

## pour vérifier, une fois que l'image est supprimée en local on peut faire un pull pour vérifier que tout est OK
docker pull localhost:5000/mynode_dockerfile
```


## RESEAU

`docker run <IDContainer> <portHost>:<portContainer>`



```bash
docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES

docker ps -a
CONTAINER ID   IMAGE              COMMAND                  CREATED          STATUS                     PORTS     NAMES


docker build -f .\Dockerfile3  --tag nginx_dockerfile .
[+] Building 0.2s (7/7) FINISHED
 => [internal] load build definition from Dockerfile3                                                              0.0s
 => => transferring dockerfile: 32B                                                                                0.0s
 => [internal] load .dockerignore                                                                                  0.0s
 => => transferring context: 2B                                                                                    0.0s
 => [internal] load metadata for docker.io/library/nginx:latest                                                    0.0s
 => [internal] load build context                                                                                  0.0s
 => => transferring context: 303B                                                                                  0.0s
 => CACHED [1/2] FROM docker.io/library/nginx                                                                      0.0s
 => [2/2] ADD [./nginx.conf, /etc/nginx/nginx.conf]                                                                0.0s
 => exporting to image                                                                                             0.0s
 => => exporting layers                                                                                            0.0s
 => => writing image sha256:342f31ded107d9d61749608eb5678d75ff5c3d78f33bfe736c657c87c22c83c3                       0.0s
 => => naming to docker.io/library/nginx_dockerfile                                                                0.0s

Use 'docker scan' to run Snyk tests against images to find vulnerabilities and learn how to fix them




docker images
REPOSITORY                         TAG       IMAGE ID       CREATED          SIZE
nginx_dockerfile                   latest    342f31ded107   17 minutes ago   142MB
localhost:5000/mynode_dockerfile   1.2       28066e06c5ab   2 hours ago      1GB
localhost:5000/mynode_dockerfile   latest    28066e06c5ab   2 hours ago      1GB
node_commited                      latest    951363b76f50   3 hours ago      998MB
node                               latest    057129cb5d6f   11 days ago      998MB
nginx                              latest    55f4b40fe486   11 days ago      142MB
registry                           2         773dbf02e42e   5 weeks ago      24.1MB


docker run -d -p 8081:8080 localhost:5000/mynode_dockerfile
9f96f69d9cfe1a129499a7cea7be92df31caf608408dbe22a8911c152491e58d

docker ps
CONTAINER ID   IMAGE                              COMMAND                  CREATED         STATUS         PORTS                    NAMES
9f96f69d9cfe   localhost:5000/mynode_dockerfile   "/bin/sh -c 'npm sta…"   8 seconds ago   Up 5 seconds   0.0.0.0:8081->8080/tcp   loving_tu



docker run -d -p 8082:8080 localhost:5000/mynode_dockerfile
b651731f61d4cba9bfcdd0d7fa339fbee29126c71f0f39d518832098473ed7db


docker ps
CONTAINER ID   IMAGE                              COMMAND                  CREATED         STATUS         PORTS                    NAMES
b651731f61d4   localhost:5000/mynode_dockerfile   "/bin/sh -c 'npm sta…"   6 seconds ago   Up 5 seconds   0.0.0.0:8082->8080/tcp   strange_perlman
9f96f69d9cfe   localhost:5000/mynode_dockerfile   "/bin/sh -c 'npm sta…"   2 minutes ago   Up 2 minutes   0.0.0.0:8081->8080/tcp   loving_tu



docker build -f .\Dockerfile3  --tag nginx_dockerfile .
[+] Building 1.6s (7/7) FINISHED
 => [internal] load build definition from Dockerfile3                                                                              0.7s
 => => transferring dockerfile: 32B                                                                                                0.0s
 => [internal] load .dockerignore                                                                                                  1.0s
 => => transferring context: 2B                                                                                                    0.0s
 => [internal] load metadata for docker.io/library/nginx:latest                                                                    0.0s
 => [internal] load build context                                                                                                  0.2s
 => => transferring context: 303B                                                                                                  0.0s
 => CACHED [1/2] FROM docker.io/library/nginx                                                                                      0.0s
 => [2/2] ADD [./nginx.conf, /etc/nginx/nginx.conf]                                                                                0.0s
 => exporting to image                                                                                                             0.0s
 => => exporting layers                                                                                                            0.0s
 => => writing image sha256:a694c7ef2d87aa35bdbfc9be289c6eff243ee0596fc1dc774f0eceab5bda12f8                                       0.0s
 => => naming to docker.io/library/nginx_dockerfile                                                                                0.0s

Use 'docker scan' to run Snyk tests against images to find vulnerabilities and learn how to fix them




docker run -d -p 80:80 nginx_dockerfile
a33bda97f2df4badd9b45fe58297a0b7dd661310af112775cc78416784865779

docker ps
CONTAINER ID   IMAGE                              COMMAND                  CREATED          STATUS          PORTS                    NAMES
a33bda97f2df   nginx_dockerfile                   "/docker-entrypoint.…"   4 seconds ago    Up 3 seconds    0.0.0.0:80->80/tcp       elegant_hellman
b651731f61d4   localhost:5000/mynode_dockerfile   "/bin/sh -c 'npm sta…"   22 seconds ago   Up 21 seconds   0.0.0.0:8082->8080/tcp   strange_perlman
9f96f69d9cfe   localhost:5000/mynode_dockerfile   "/bin/sh -c 'npm sta…"   3 minutes ago    Up 3 minutes    0.0.0.0:8081->8080/tcp   loving_tu


## en se connectant sur l'url localhost:80 et en raffraichissant régulièrement on voit bien appraitre les IDs des 2 containers
```



#### creation de network
`docker network create <nomReseau>`
`docker network connect <nomReseau>`
`docker network ls`
`docker network rm <nomReseau>`
`docker network disconnect <nomReseau>`
`docker network ??`


```bash
docker network ls
--------------------------
NETWORK ID     NAME      DRIVER    SCOPE
2a2745a04293   bridge    bridge    local
749fae787d7e   host      host      local
468b08afb5ad   none      null      local
--------------------------

## on crée un nouveau reseau
docker network create formation

## pour recréer les neouds sur un nouveau reseau on modifie le fichier nginx2.conf
## ET on relance les docker build avec l'option --net=formation

docker run -d  --name node1 --net=formation localhost:5000/mynode_dockerfile
docker run -d  --name node2 --net=formation localhost:5000/mynode_dockerfile
docker run -d -p 80:80 --net=formation nginx_dockerfile4

```
En mentionnant `--name <nomMachine>` et en renseignant ce nom dans le fichier de conf de nginx on a un dns qui est pris en compte.


## LES VOLUMES
[docker volume](https://docs.docker.com/engine/reference/commandline/volume/)

syntaxe
`docker run -v <host_path>:<container_path> <nomContainer>`

> on peut aussi utiliser l'argument `--volume` à la place de `-v `

Le container qui partage le volume partage avec `-v`
1 conteneur qui veut récupérer le partage de volume d'un autre conteneur avec `--volumes-from`

exemple: volume partage sur container A et container B monte ce partage
`docker run -v <nomVolumeConatinerA> containerA`
`docker run --volumes-from containerA containerB`


`docker volume create <nomVolume>`
`docker volume ls`
`docker volume inspect <nomVolume>`
`docker volume rm <nomVolume>`


```bash
docker run --net=formation -v C:\Users\Administrateur\formationDocker\volumePartage:/data --name redis -d redis redis-server --appendonly yes

run -d --net=formation --name node1 node_dockerfile6
run -d --net=formation --name node2 node_dockerfile6


docker ps
CONTAINER ID   IMAGE              COMMAND                  CREATED         STATUS         PORTS      NAMES
982c58c282a4   node_dockerfile6   "/bin/sh -c 'npm sta…"   2 minutes ago   Up 2 minutes   8080/tcp   node2
e9d7dab6a6f7   node_dockerfile6   "/bin/sh -c 'npm sta…"   2 minutes ago   Up 2 minutes   8080/tcp   node1
9985f9d90864   redis              "docker-entrypoint.s…"   4 minutes ago   Up 4 minutes   6379/tcp   redis


```


creation de volume
```bash
docker volume create redis-stockage
  redis-stockage

docker volume ls
DRIVER    VOLUME NAME
local     2efc7a04d1d19b7f2c6cc2fde9f6c207bc7187afd6dfc63848eab597f672475d
local     1698bea05c13bd862a10dc72a27f2ab97c3d9a0cff805c0f04446908911b69ce
local     dc4d434a4aa65255d8235fb438c2f7fe3d273c0647ab352bc9d9d20e9a2d8e32
local     redis-stockage

docker run --net=formation -v redis-stockage:/data --name redis -d redis redis-server --appendonly yes
539136dd7796a69b25a3baadf8f954e868206925b55e619d645a3852630de6f6


docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS      NAMES
539136dd7796   redis     "docker-entrypoint.s…"   9 seconds ago   Up 5 seconds   6379/tcp   redis


docker run -it -v redis-volume:/montestvolumepartage ubuntu bash
Unable to find image 'ubuntu:latest' locally
latest: Pulling from library/ubuntu
405f018f9d1d: Pull complete
Digest: sha256:b6b83d3c331794420340093eb706a6f152d9c1fa51b262d9bf34594887c2c7ac
Status: Downloaded newer image for ubuntu:latest


root@645090713d50:/# ls -la montestvolumepartage/
total 8
drwxr-xr-x 2 root root 4096 Jul  5 08:04 .
drwxr-xr-x 1 root root 4096 Jul  5 08:04 ..


docker volume inspect redis-volume
[
   {
       "CreatedAt": "2022-07-05T08:04:32Z",
       "Driver": "local",
       "Labels": null,
       "Mountpoint": "/var/lib/docker/volumes/redis-volume/_data",
       "Name": "redis-volume",
       "Options": null,
       "Scope": "local"
   }
]

```

## DOCKER MACHINE

Docker-machine sert à créer des VMs sur notre hosts lorsque nous n'avons pas de SI à notre disposition.
L'utilisation de docker-machine est décrite plus bas dans la partie docker swarm.


## DOCKER COMPOSE
[docker compose](https://docs.docker.com/compose/gettingstarted/)
[docker compose reference](https://docs.docker.com/compose/compose-file/)

Docker compose permet de simplifier toutes les commandes de build et run en 1 seul fichier.

`docker build -t web --f dockerfile-alternate .`
est équivalent à
```yml
web:
  build:
    context:
      dockerfile: dockerfile-alternate
```



`docker run -p 5000:5000 -v .:/code web`
est équivalent à
```yml
web:
  ports:
    - "5000:5000"
  volumes:
    .:/code
```


##### TP7
Une fois les fichiers créés on lance `docker-compose up -d`

```bash
docker-compose up -d

docker-compose ps
          Name                         Command               State     Ports
------------------------------------------------------------------------------
tp7-dockercompose_nginx_1   /docker-entrypoint.sh ngin ...   Exit 1
tp7-dockercompose_node1_1   /bin/sh -c npm start             Up       8080/tcp
tp7-dockercompose_node2_1   /bin/sh -c npm start             Up       8080/tcp
tp7-dockercompose_redis_1   docker-entrypoint.sh --app ...   Up       6379/tcp
```


Pour supprimer le cluster du docker-compose
```bash
docker-compose kill
Killing tp7-dockercompose_node1_1 ... done
Killing tp7-dockercompose_node2_1 ... done
Killing tp7-dockercompose_redis_1 ... done

docker-compose rm
Going to remove tp7-dockercompose_nginx_1, tp7-dockercompose_node1_1, tp7-dockercompose_node2_1, tp7-dockercompose_redis_1
Are you sure? [yN] y
Removing tp7-dockercompose_nginx_1 ... done
Removing tp7-dockercompose_node1_1 ... done
Removing tp7-dockercompose_node2_1 ... done
Removing tp7-dockercompose_redis_1 ... done
```
Maintenant nous n'avons plus aucun container en running


>Remarque: lorsqu'on relance le build des images avec docker-compose les images portent le noms des services

```bash
docker images
REPOSITORY                         TAG       IMAGE ID       CREATED          SIZE
tp7-dockercompose_nginx            latest    181dac1922af   15 minutes ago   142MB
tp7-dockercompose_node2            latest    e05e5478025e   3 hours ago      1GB
tp7-dockercompose_node1            latest    e05e5478025e   3 hours ago      1GB
redis                              latest    2e50d70ba706   11 days ago      117MB
node                               latest    057129cb5d6f   12 days ago      998MB
nginx                              latest    55f4b40fe486   12 days ago      142MB
registry                           2         773dbf02e42e   5 weeks ago      24.1MB
```



## DOCKER SWARM

Il faut 2N+1 noeuds manager et des noeuds workers

On va créer des machines virtuelles avec `docker-machine` pour créer les noeuds qui nous allons utiliser.

#### Installation de docker-machine
[install docker machine](https://docker-docs.netlify.app/machine/install-machine/#install-machine-directly)

docker machine nous permet de créer des VMs sur notre host ayant docker d'installé.

création de noeuds avec docker-machine (sur windows qui utilise hyper-V) et MySwitch a été créé dans hyper-V
```bash
## au cours de la formation nous sommes sur windows nous installons chocolatey pour pouvoir installer facilement docker-machine

choco install -y docker-machine

#desactiver openssh dans param/aaplicationsfonctinalités facultatives

docker-machine --debug create --driver hyperv --hyperv-virtual-switch "MySwitch" node1
docker-machine --debug create --driver hyperv --hyperv-virtual-switch "MySwitch" node2
docker-machine --debug create --driver hyperv --hyperv-virtual-switch "MySwitch" node3

#sur node1:
ifconfig
docker swarm init --advertise-addr 10.140.25.159 --listen-addr 10.140.25.159

#sur node2 et node3:
docker swarm join --token SWMTKN-1-1yrrfka1rpx1hslrx9ag0rdvx4x877yl9kij7owd9g5xr11sf7-c0lddw698bbkrcitlth20dl79 10.140.25.159:2377



#sur le host: (pour vérifier qu on point sur le noeud manager: node1) DOCKER_HOST
docker-machine env node1
$Env:DOCKER_TLS_VERIFY = "1"
$Env:DOCKER_HOST = "tcp://10.140.25.159:2376"
$Env:DOCKER_CERT_PATH = "C:\Users\Administrateur\.docker\machine\machines\node1"
$Env:DOCKER_MACHINE_NAME = "node1"
$Env:COMPOSE_CONVERT_WINDOWS_PATHS = "true"



#sur le node1:
docker node ls
ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
6mdbrewo5w8n806xb08qbo2hl *   node1               Ready               Active              Leader              19.03.12
v3bjckvuu3fcv08c6isxtamf5     node2               Ready               Active                                  19.03.12
kefdpdvx6gwlyh5mkep35ddd5     node3               Ready               Active                                  19.03.12
## dans notre cas on est sur le noeud 1 qui est le noeud master (donc le docker  node ls s'ffiche et une "*" apparait sur le noeud surlquel nous sommes)
### si on n'est pas sur ce noeud (depuis le host) on refait les lignes du dessus ou la dernière qui s'affiche
### `& "C:\ProgramData\chocolatey\lib\docker-machine\bin\docker-machine.exe" env node1 | Invoke-Expression`


#sur le host: (on peut faire ça après avoir fait les commandes du DOCKER_HOST (on voit du host comme si on est dans le noeud manager)
docker node ls
ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
6mdbrewo5w8n806xb08qbo2hl *   node1               Ready               Active              Leader              19.03.12
v3bjckvuu3fcv08c6isxtamf5     node2               Ready               Active                                  19.03.12
kefdpdvx6gwlyh5mkep35ddd5     node3               Ready               Active                                  19.03.12
```



```bash
## création du reseau logging pour elk
docker network create -d overlay logging
t4qxl34k92ij22d6epfou5rq4

docker network ls
NETWORK ID     NAME              DRIVER    SCOPE
16cc7e856a70   bridge            bridge    local
6263e14d11f8   docker_gwbridge   bridge    local
e610ebcfdb98   host              host      local
h45yzo1eozb7   ingress           overlay   swarm
t4qxl34k92ij   logging           overlay   swarm
7a1f4ec0385d   none              null      local

## mise en place du service elasticsearch
docker service create --name=elasticsearch --network=logging -p 9200:9200 -p 9300:9300  -e "xpack.security.enabled=false" -e "xpack.security.http.ssl.enabled=false" -e "xpack.security.transport.ssl.enabled=false" -e "cluster.initial_master_nodes=elasticsearch" -e "node.name=elasticsearch" elasticsearch:8.3.1
60pgrr8bj9qrmhzeugnz2i9nq
overall progress: 1 out of 1 tasks
1/1: running   [==================================================>]
verify: Service converged


docker service ls
ID             NAME            MODE         REPLICAS   IMAGE                 PORTS
60pgrr8bj9qr   elasticsearch   replicated   1/1        elasticsearch:8.3.1   *:9200->9200/tcp, *:9300->9300/tcp

# se connecter sur chaque noeud pour modifier la conf (minimum nécessaire pour que elk fonctionne)
docker-machine ssh node1 | node2 | node3
docker@node1:~$ sudo sysctl -w vm.max_map_count=262144
vm.max_map_count = 262144


## mise en place du service kibana
docker service create --name=kibana --network=logging -p 5601:5601 -e "ELASTICSEARCH_HOSTS: http://elasticsearch:9200" kibana:8.3.1

docker service ls
ID             NAME            MODE         REPLICAS   IMAGE                 PORTS
60pgrr8bj9qr   elasticsearch   replicated   1/1        elasticsearch:8.3.1   *:9200->9200/tcp, *:9300->9300/tcp
x9nao3u6z024   kibana          replicated   1/1        kibana:8.3.1          *:5601->5601/tcp

## pour vérifier si le service kibana fonctionne et connaitre le noeud sur lesquel il tourne
docker service ps kibana
ID             NAME           IMAGE          NODE      DESIRED STATE   CURRENT STATE           ERROR                              PORTS
r3ggtiss857e   kibana.1       kibana:8.3.1   node3     Running         Running 17 hours ago


# pour connaitre l'IP des noeuds
docker-machine ls
NAME    ACTIVE   DRIVER   STATE     URL                        SWARM   DOCKER      ERRORS
node1   *        hyperv   Running   tcp://10.140.25.159:2376           v19.03.12
node2   -        hyperv   Running   tcp://10.140.25.171:2376           v19.03.12
node3   -        hyperv   Running   tcp://10.140.25.161:2376           v19.03.12

## on se connecte sur l'IP du noeud sur lesquel kibana tourne sur le port 5601 qu'on a definit
#[kibana](http://10.140.25.161:5601/)



## mise en place du service logstash
## on crée le docekr-compose pour logstash et on lance la creation du service logstash
cd <repertoire où est le docker-compose.yml de logstash>

docker stack deploy -c .\docker-compose.yml logstash
Creating service logstash_logstash


docker service ls
ID             NAME                MODE         REPLICAS   IMAGE                 PORTS
owls9fxj9avg   elasticsearch       replicated   1/1        elasticsearch:8.3.1   *:9200->9200/tcp, *:9300->9300/tcp
x9nao3u6z024   kibana              replicated   1/1        kibana:8.3.1          *:5601->5601/tcp
je9rzyaw2yry   logstash_logstash   replicated   1/1        logstash:8.3.1


docker service ps logstash_logstash
ID             NAME                  IMAGE            NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
t25uxgdn1dzf   logstash_logstash.1   logstash:8.3.1   node2     Running         Running 49 seconds ago


## on cherche sur quel noeud tourne logstash
docker service ps logstash_logstash
ID             NAME                  IMAGE            NODE      DESIRED STATE   CURRENT STATE           ERROR     PORTS
t25uxgdn1dzf   logstash_logstash.1   logstash:8.3.1   node2     Running         Running 2 minutes ago


## on se connecte à kibana et on va dans la barre de recherche Data Views
## si logstash demande une creation de data view on note log* dans le champ name et create data view
## on va dans discovery et on voit les logs qui remmontent
## en cherchant le champ hostname dans kibana on voit l'ID du container logstash qui tourne
##pour véirifer on va sur le noeud où tourne logstash et on fait un docker ps
docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                NAMES
44407031fcc3        logstash:8.3.1      "/usr/local/bin/dock…"   3 minutes ago       Up 3 minutes        5044/tcp, 9600/tcp   logstash_logstash.1.t25uxgdn1dzf96r59eude48k4

## l'ID est bien le même

```

Mettre opération avec 1 seul fichier (elk.yml)

```bash
docker stack deploy -c .\elk.yml elk

Creating network elk_logging
Creating service elk_elasticsearch
Creating service elk_kibana
Creating service elk_logstash


docker service ls
ID             NAME                MODE         REPLICAS   IMAGE                 PORTS
owg9nzwv1bhc   elk_elasticsearch   replicated   1/1        elasticsearch:8.3.1   *:9200->9200/tcp, *:9300->9300/tcp
ag0rj3rwnace   elk_kibana          replicated   1/1        kibana:8.3.1          *:5601->5601/tcp
15grsjze9ht0   elk_logstash        replicated   1/1        logstash:8.3.1

## on se connecte à kibana et on fait comme précédemment
```


## CONCEPTS AVANCES



## FIN
