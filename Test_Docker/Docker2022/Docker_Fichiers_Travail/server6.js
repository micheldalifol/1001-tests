'use strict';
const express = require('express');
const os = require("os");
const redis = require('redis');
var client = redis.createClient('6379', 'redis');
const app = express();
app.get('/', function (req, res) {
  client.incr('counter', function(err, counter) {
    if(err) return next(err);
    res.send('Hello from ' + os.hostname() + '\n page views' +
counter + ' times!');
  });
});
app.listen(8080);
